**Java Inicio**

**Descripcion**
La idea de este proyecto es que puedan ver lo mas basico del lenguaje Java hasta llegar a una conexion de base de datos y 
manejar transaccionalidad (Para ello se crearia un ABM).

**src/clase1**
Podras encontrar lo referente a los tipos de datos que maneja Java para declarar sus variables, ademas de los condicionales y
ciclos repetitivos.

**src/clase2**
En el folder encontraran ejemplos de clase, objeto, atributos, métodos y el uso de los mismos para un proceso de inscripcion
de estudiantes con su respectivo curso.

**src/clase3**
En este folder veremos el funcionamiento de la herencia, upcasting, polimorfismo, ademas del uso de constructores.

**src/clase4**
En este folder encontraras todo lo referente a clases abstractas, polimorfismo, interface, sobrecarga y modificadores de acceso.

**src/clase5**
 * Introducción al uso de excepciones
 * Bloques try, catch, finally
 * La clase Exception
 * Tipos de excepciones: checked vs. unchecked
 * Excepciones construidas por el usuario
 * Palabras clave throw y throws

**src/clase6**
 * Una introducción a JDBC
 * Introducción a SQL
 * Sentencias SELECT, INSERT, UPDATE, DELETE

**src/clase7**
 * Introducción a JDBC 
 * Como realizar operaciones SELECT, INSERT, UPDATE, DELETE desde Java
 * La clase PreparedStatement
 * Utilización de excepciones de base de datos
 * Commit y rollback
 * DAO: Data Access Object

**src/clase8_9**
 * Introducción a las colecciones
 * Interfaces List, Set
 * Diferentes clases para utilizar colecciones
 * Formas de iterar a través de colecciones
 * Introducción a las colecciones màs nuevas
 * Interfaces Queue, Map
 * Diferentes clases para utilizar colecciones
 * Formas de ordenar colecciones

**src/clase10_11**
 * Una introducción al mundo de java web
 * Que es un servlet
 * Una introducción a jsp
 * Desarrollo de la última fase del proyecto integrador
 * Integración del proyecto con base de datos
 
Para ver proyecto ejemplo de los puntos anteriores visitar la pagina **https://gitlab.com/fernandoroa/java-web**

**src/clase12**
 * Introducción al uso de streams
 * Streams orientados a carácter y streams orientados a texto
 * InputStream, OutputStream, Reader, Writer
 * Utilización de Buffers
 * Uso avanzado de streams
 * Variantes de utilización de los Buffers
 * Streams en conjunto con interfaz gráfica
 
**src/clase13**
 * Elementos avanzados que salieron en las últimas versiones de java
 * Que son las java lamda
 * Que son los streams

**documentos**
En esta carpeta encontraran ejercicios para practicar y verificar la compresion de todo lo visto en las subcarpetas de src. 

**Importante**
El contenido del repositorio fue creado en  el IDE Eclipse por ende si requieren utilizar otro
IDE para programar, entonces deberan descargar el repositorio y copiar el contenido
de la carpeta "src" en el "src" del proyecto que han creado o importar el contenido
de la misma a traves del  IDE.

En algunos ejercicios se haran mencion del uso de Java 8 para ir entendiendo el uso de la programacion funcional.