package clase12.ejercicios.logs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LogsPrograma {

	public static void main(String[] args) throws IOException {
		try {
			int[] arreglo = new int[5];
			for (int i = 0; i < 10; i++) {
				arreglo[i] = 0;
			}
		} catch (Exception ex) {
			new LogsPrograma().escribirLog(ex.toString());
		}
	}

	private static void escribirLog(String error) throws IOException {
		File archivo = new File("logs.txt");
		if (archivo.exists()) {
			BufferedReader reader = new BufferedReader(new FileReader(archivo));
			StringBuffer stringBuffer = new StringBuffer();
			String sCurrentLine;
			while ((sCurrentLine = reader.readLine()) != null) {
				stringBuffer.append(sCurrentLine).append("\n");
			}
			stringBuffer.append(error);
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(archivo));
			bufferedWriter.write(stringBuffer.toString());
			bufferedWriter.close();
			reader.close();
		} else {
			BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(archivo));
			bufferedWriter.write(error);
			bufferedWriter.close();
		}

	}

}
