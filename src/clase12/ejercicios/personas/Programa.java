package clase12.ejercicios.personas;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Programa {

	public static void main(String[] args) throws IOException {
		BufferedWriter outMenores = new BufferedWriter(new FileWriter("menores.txt"));
		BufferedWriter outMayores = new BufferedWriter(new FileWriter("mayores.txt"));
		ArrayList<Alumno> alumnos = new ArrayList<>();
		alumnos.add(new Alumno("ADRIANA", "ROA", 12));
		alumnos.add(new Alumno("EMILIA", "SANCHEZ", 19));
		alumnos.add(new Alumno("JOSE", "CAPPONI", 21));
		alumnos.add(new Alumno("AGUSTIN", "MARTIN", 8));
		alumnos.add(new Alumno("ROMINA", "DIAZ", 11));
		alumnos.add(new Alumno("PABLO", "PEREZ", 40));

		String menores = "";
		String mayores = "";
		for (Alumno alumno : alumnos) {
			if (alumno.getEdad() < 18) {
				menores = menores.concat(alumno.toString()).concat("\n");
			} else {
				mayores = mayores.concat(alumno.toString()).concat("\n");
			}
		}

		outMenores.write(menores);
		outMayores.write(mayores);
		outMayores.close();
		outMenores.close();
	}

}
