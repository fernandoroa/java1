package clase12.escribir;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Escribir {

	public static void main(String[] args) throws IOException {

		// Define el archivo a utilizar
		File archivoSalida = new File("destino.txt");

		// Abre el stream necesario
		FileWriter out = new FileWriter(archivoSalida);

		// Define la información a guardar en el archivo
		String info = "Bueno y si aprendemos java";

		// Escribe el archivo con la información

		for (int i = 0; i < info.length(); i++)
			out.write(info.charAt(i));
		// Cierra los streams

		out.close();
	}
}
