package clase12.leer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReaderPrograma {
	
	public static void main(String[] args) throws IOException {
		
		File archivo = new File("fuente.txt");

		BufferedReader in  = new BufferedReader(new FileReader(archivo));

		// Lee la primera linea del archivo fuente.txt a traves del stream
		// denominado in
		String lineaLeida = in.readLine();
		System.out.println(lineaLeida);

		// Cierra el stream y libera recursos
		in.close();
	}

}
