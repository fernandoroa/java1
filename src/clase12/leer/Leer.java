package clase12.leer;

import java.io.FileReader;
import java.io.IOException;

public class Leer {

    public static void main(String[] args) throws IOException {
        // Define el archivo a utilizar
        try (FileReader archivoEntrada = new FileReader("fuente.txt")) {
            int unCaracter;
            while ((unCaracter = archivoEntrada.read()) != -1) {
                System.out.println((char) unCaracter);
            }
            System.out.println("ar.com.educacionit.lector.Leer.main()"+ archivoEntrada.read());
        } catch (Exception ex) {
            throw new IOException("el archivo no pudo ser encontrado"+ex.getMessage());
        }

    }
}
