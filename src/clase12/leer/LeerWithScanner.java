package clase12.leer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class LeerWithScanner {
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		File archivoEntrada = new File("fuente.txt");

		// Abre el stream necesario
		FileReader in = new FileReader(archivoEntrada);
		
		//Lectura por un medio de entrada, en este caso  es el FileReader
		Scanner scanner = new Scanner(in);
		System.out.println(scanner.nextInt());
		System.out.println(scanner.next());
		in.close();
		
	}

}
