package clase12.leer_escribir;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class LecturaEscritura {

	public static void main(String[] args) throws IOException {
		File archivoEntrada = new File("fuente.gif");

		File archivoSalida = new File("destino.gif");

		// Abre los streams necesarios
		FileInputStream in = new FileInputStream(archivoEntrada);
		FileOutputStream out = new FileOutputStream(archivoSalida);

		int unCaracter;

		// Copia el archivo fuente en el archivo destino

		while ((unCaracter = in.read()) != -1)

			out.write(unCaracter);

		// Cierra los streams

		in.close();
		out.close();
	}
}
