package clase13.advanced;

import java.util.ArrayList;

public class Main {
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
		Saludo saludo1 = (str) -> "Buen dia " + str + "!";
		Saludo saludo2 = (str) -> "Buenas tardes " + str + "!";
	  
	  	// Output: Good Morning Luis! 
		System.out.println(saludo1.saluda("Luis"));
		
		// Output: Good Evening Jessica!
		System.out.println(saludo2.saluda("Pepe"));
		
		Calculadora cal = (argA, argB) -> argA + argB;
		
		System.out.println(cal.suma(10, 23));
		
		ArrayList<String> array = new ArrayList<String>() {

			public boolean contains(Object o) {
				return true;
				};
		};
		array.add("algo");
		System.out.println(array.contains("Hola mundo"));
		
		Saludo reverseStr = (str) -> {
			String result = "";
			
			for(int i = str.length()-1; i >= 0; i--)
				result += str.charAt(i);
			
			return result;
		};
		System.out.println(reverseStr.saluda("Lambda Demo")); 
		

	}

}
