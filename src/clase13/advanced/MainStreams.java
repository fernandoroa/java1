package clase13.advanced;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class MainStreams {

	public static void main(String[] args) {

		ArrayList<Persona> lista = new ArrayList<Persona>();
		lista.add(new Persona("Pepe"));
		lista.add(new Persona("Juan"));
		lista.add(new Persona("Sofia"));

		Collections.sort(lista, (Persona p1, Persona p2) -> p1.getNombre().compareTo(p2.getNombre()));

		for (Persona p : lista) {

			System.out.println(p.getNombre());

		}

		List<String> myList = Arrays.asList("a1", "a2", "b1", "c2", "c1");

		myList.stream().filter(s -> s.startsWith("c")).map(String::toUpperCase).sorted().forEach(System.out::println);

		IntStream.range(1, 4).forEach(System.out::println);

		Arrays.asList("a1", "a2", "a3").stream().findFirst().ifPresent(System.out::println); // a1

		Stream.of(1.0, 2.0, 3.0).mapToInt(Double::intValue).mapToObj(i -> "a" + i).forEach(System.out::println);

		List<String> paises = Arrays.asList("Colombia", "M�xico", "Guatemala");
		// Stream de enteros cuyos elementos son el num de caracteres de los pa�ses
		IntStream valor = paises.stream().mapToInt(String::length);
		valor.forEach(System.out::println);

		Stream.of("d2", "a2", "b1", "b3", "c").filter(s -> {
			System.out.println("filter: " + s);
			return true;
		}).forEach(s -> System.out.println("forEach: " + s));

		Stream<String> stream = Stream.of("d2", "a2", "b1", "b3", "c").filter(s -> s.startsWith("a"));

		stream.forEach(System.out::println);
		

	}
}
