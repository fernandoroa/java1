package clase13.advanced;

public class Persona {
	private String nombre;

	public Persona(String string) {
		setNombre(string);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
