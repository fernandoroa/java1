package clase13.creacionobjetos;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CreacionDeObjetos {

	public static void main(String[] args) {
		List<Error> errors = new ArrayList<>();
		errors.add(new Error("1", "Problema al intentar conectar con el servicio"));
		errors.add(new Error("2", "Numero de documento incorrecto"));
		List<InfoStatus> infoStatus = errors.stream().map(x -> new InfoStatus(x.getCod(), x.getDescripcion()))
				.collect(Collectors.toList());
		
		List<InfoStatus> infoStatusSinEstado2 = errors.stream().map(x -> new InfoStatus(x.getCod(), x.getDescripcion())).filter(x-> !"2".equalsIgnoreCase(x.getCodigo()))
				.collect(Collectors.toList());

		infoStatus.stream().forEach(x -> System.out.println(x.getCodigo() + " " + x.getDescripcion()));
		System.out.println("-------------------------------------------------");
		infoStatusSinEstado2.stream().forEach(x -> System.out.println(x.getCodigo() + " " + x.getDescripcion()));

	}

}
