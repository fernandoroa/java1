package clase13.creacionobjetos;

public class Error {

	String cod;
	String descripcion;

	public Error(String cod, String descripcion) {
		super();
		this.cod = cod;
		this.descripcion = descripcion;
	}

	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
