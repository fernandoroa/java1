package clase13.estudiantesedad;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EstudiantesPrograma {

	public static void main(String[] args) {
		List<Integer> edadesList = Arrays.asList(new Integer[] { 11, 12, 14, 20, 25, 30, 45, 55 });
		List<Integer> menoresDeEdad = edadesList.stream().filter(x -> x <= 18).collect(Collectors.toList());
		List<Integer> mayoresDeEdad = edadesList.stream().filter(x -> x > 18).collect(Collectors.toList());
		System.out.println("Menores de edad : " + menoresDeEdad);
		System.out.println("Mayores de edad : " + mayoresDeEdad);
	}

}
