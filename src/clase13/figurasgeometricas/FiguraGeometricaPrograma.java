package clase13.figurasgeometricas;

import clase6.ejercicios.figurasgeometricas.Circulo;
import clase6.ejercicios.figurasgeometricas.Cuadrado;
import clase6.ejercicios.figurasgeometricas.Triangulo;

public class FiguraGeometricaPrograma {

	public static void main(String[] args) {

		Triangulo triangulo = new Triangulo(10, 2);
		Cuadrado cuadrado = new Cuadrado(10);
		Circulo circulo = new Circulo(10);
		FiguraGeometrica trianguloArea = figura -> figura + " Area: " + (triangulo.getBase() * triangulo.getAltura()) / 2;
		FiguraGeometrica cuadradoArea = figura -> figura + " Area: " + cuadrado.getLado() * cuadrado.getLado();
		FiguraGeometrica circuloArea = figura -> figura + " Area: " + Math.PI * Math.pow(circulo.getRadio(), 2);
		
		System.out.println(trianguloArea.area("Triangulo"));
		System.out.println(cuadradoArea.area("Cuadrado"));
		System.out.println(circuloArea.area("Circulo"));

	}

}
