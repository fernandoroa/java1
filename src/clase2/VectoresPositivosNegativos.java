/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2;

/**
 *
 * @author Fernando
 */
public class VectoresPositivosNegativos {

    public static void main(String[] args) {

        int totalPositivos = 0;
        int totalNegativos = 0;
        int[] vecNumeros = new int[]{11, -22, 33, -44, 55, -66, 77, -88, 99};
        int[] vecPositivos = new int[vecNumeros.length];
        int[] vecNegativos = new int[vecNumeros.length];

        for (int i = 0; i < vecNumeros.length; i++) {
            if (vecNumeros[i] < 0) {
                vecNegativos[i] = vecNumeros[i];
                totalNegativos += vecNumeros[i];
            } else {
                vecPositivos[i] = vecNumeros[i];
                totalPositivos += vecNumeros[i];
            }
        }

        for (int i = 0; i < vecNumeros.length; i++) {
            System.out.println("vecNumeros " + (i + 1) + " : " + vecNumeros[i]);
        }
        System.out.println("\n################################################## \n");
        for (int i = 0; i < vecPositivos.length; i++) {
            if (vecPositivos[i] > 0) {
                System.out.println("vecPositivos " + (i + 1) + " : " + vecPositivos[i]);
            }
        }
        System.out.println("\n################################################## \n");

        for (int i = 0; i < vecNegativos.length; i++) {
            if (vecNegativos[i] < 0) {
                System.out.println("vecNegativos " + (i + 1) + " : " + vecNegativos[i]);
            }
        }
        System.out.println("\n################################################## \n");
        System.out.println("totalPositivos :" + totalPositivos);
        System.out.println("totalNegativos :" + totalNegativos);
    }

}
