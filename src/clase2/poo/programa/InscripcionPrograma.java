/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2.poo.programa;


import java.util.ArrayList;
import java.util.List;

import clase2.poo.models.Curso;
import clase2.poo.models.Estudiante;
import clase2.poo.models.Inscripcion;

/**
 *
 * @author Fernando
 */
public class InscripcionPrograma {

    public static void main(String[] args) {
        Inscripcion inscripcion = new Inscripcion();
        inscripcion.setCurso(new Curso(1, "Java Standard", "Lun-Vie"));
        inscripcion.setEstudiante(new Estudiante(0, "Pedro", "Carrasco", 18, 120));

        System.out.println("contenido inscripcion : " + inscripcion.getEstudiante().getNombre());

        List<Estudiante> estudiantes = new ArrayList<>();
        estudiantes.add(new Estudiante(1, "pedro", "perez", 21, 170));
        estudiantes.add(new Estudiante(2, "juan", "gordo", 21, 170));
        estudiantes.add(new Estudiante(3, "martina", "denigris", 21, 170));
        estudiantes.add(new Estudiante(4, "romina", "caponi", 21, 170));
        estudiantes.add(new Estudiante(5, "paula", "perez", 21, 170));

        
        for (int i = 0; i < estudiantes.size(); i++) {
            System.out.println("for viejito -> "+estudiantes.get(i).toString());
        }
        
        for (Estudiante estudiante : estudiantes) {
            System.out.println("forEach -> "+ estudiante.toString());

        }
        
        //Java 8
        estudiantes.stream().forEach((estudiante) -> System.out.println("For Java8 -> "+estudiante.toString()));
    }

}
