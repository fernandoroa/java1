/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2.poo.programa;


import java.util.Scanner;

import clase2.poo.models.Estudiante;

/**
 *
 * @author Fernando
 */
public class InscripcionScanner {
    
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        Estudiante estudiante = new Estudiante();
        
        System.out.print("ingrese edad : ");
        estudiante.setEdad(scanner.nextInt());
       
        System.out.print("ingrese nombre : ");
        estudiante.setNombre(scanner.next());
        
        System.out.print("ingrese apellido : ");
        estudiante.setApellido(scanner.next());
        
        System.out.print("ingrese altura : ");
        estudiante.setAltura(scanner.nextInt());
        
        
        System.out.println("\n");
        System.out.println("Estudiante : " + estudiante.toString());
        
    }
    
}
