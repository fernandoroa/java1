package clase3.encapsulamiento;

import java.util.List;

/**
 * 
 * @author fernando
 *
 */
public class AvionBombardero {

	boolean salioAvion = false;
	boolean avionActivo = true;

	public void disparar(List<Misil> misiles) {
		for (Misil misil : misiles) {
			lanzarMisil(misil);
		}
		// Java 8
		// misiles.stream().forEach(misil -> lanzarMisil(misil));
	}

	private void lanzarMisil(Misil misil) {
		// El metodo puedeExplotar es el encargado de validar condiciones de explosion
		if (misil.puedeExplotar(true)) {
			System.out.println("BOOOOM...! Objetivo alcanzado - misil Nro: " + misil.getIdMisil());
			avionActivo = true;
		}
	}

}
