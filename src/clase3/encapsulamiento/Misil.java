package clase3.encapsulamiento;

/**
 * 
 * @author fernando
 * 
 *         La clase misil al poseer un constructor con variables de inicio
 *         pierde el constructor por defecto.
 */
public class Misil {

	private int idMisil;

	// Visibilidad de la variable publica por motivos de ejemplo
	public boolean explotar;

	// Contructor con variables
	public Misil(int idMisil) {
		super();
		this.idMisil = idMisil;
	}

	public int getIdMisil() {
		return idMisil;
	}

	public void setIdMisil(int idMisil) {
		this.idMisil = idMisil;
	}

	public boolean isExplotar() {
		return explotar;
	}

	/**
	 * Metodo encargado de validar si los misiles estan en buen estado
	 * @param salioDelAvion
	 * @return
	 */
	public Boolean puedeExplotar(boolean salioDelAvion) {
		if (salioDelAvion && !isExplotar()) {
			explotar = true;
			return true;
		} else if (salioDelAvion && isExplotar()) {
			System.out.println("SOS...!!!!! Busca el ParaCaidas");
			return false;
		}
		explotar = false;
		return false;
	}

}
