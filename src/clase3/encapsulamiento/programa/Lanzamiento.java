package clase3.encapsulamiento.programa;

import java.util.ArrayList;
import java.util.List;

import clase3.encapsulamiento.AvionBombardero;
import clase3.encapsulamiento.Misil;

/**
 * 
 * @author fernando
 * 
 *         Este ejercicio esta pensado para exaplicar la importancia de declarar
 *         una variable publica o privada y las consecuencias de dejar expuesto
 *         las varibles para que puedan ser modificadas sin un metodo que se
 *         encargue de tal funcion
 * 
 *         Pasos a seguir: 
 *         1.- Ejecutar el programa comentando la linea 34 
 *         2.- Ejecutar el programa con la linea 34 comentada y revisar en la
 *         Console lo sucedido
 *
 */
public class Lanzamiento {

	public static void main(String[] args) {
		AvionBombardero avionBombardero = new AvionBombardero();

		List<Misil> misiles = new ArrayList<Misil>();
		misiles.add(new Misil(1));
		misiles.add(new Misil(2));
		misiles.add(new Misil(3));

		Misil misil2 = new Misil(4);
//		 misil2.explotar = true;
		misiles.add(misil2);

		/**
		 * El metodo disparar es un ejemplo simple de encapsulamiento que priva al programador de saber que
		 * hace internamente el metodo mas alla de su aparente funcionamiento por el nombre que posee
		 * 
		 * Por motivos de estudios, revisar el funcionamiento del mismo.
		 */
		avionBombardero.disparar(misiles);
	}

}
