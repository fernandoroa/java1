package clase3.herencia;

/**
 * 
 * @author fernando
 *
 */
public class Ejecutivo extends Empleado {

	private String rol;

	
	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	/**
	 * Imprime en consola el rol ademas del nombre y apellido del empleado
	 */
	public void ejecutarFunciones() {
		// Operador condicional (ternario) que valida que el rol sea distinto de null
		String rol = getRol() != null ? getRol() : "No posee";
		System.out.println("me ejecuto - rol: " + rol + "- NombreApellido : " + getNombre() + "/" + getApellido());
	}

}
