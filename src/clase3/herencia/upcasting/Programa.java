package clase3.herencia.upcasting;

import clase3.herencia.Ejecutivo;
import clase3.herencia.Empleado;

/**
 * 
 * @author fernando
 *
 */
public class Programa {

	public static void main(String[] args) {
		
		//Aplicando upscasting 
		Empleado empleado = new Ejecutivo();
		empleado.setApellido("Ramirez");
		empleado.setId(1);
		empleado.setNombre("Mario");
		
		Ejecutivo ejecutivo = (Ejecutivo) empleado;
		ejecutivo.ejecutarFunciones();
		
		Ejecutivo ejecutivo2 = new Ejecutivo();
		ejecutivo2.setApellido("Roa");
		ejecutivo2.setNombre("Romina");
		ejecutivo2.setRol("general");
		ejecutivo2.ejecutarFunciones();
		
	}

}
