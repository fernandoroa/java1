/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.abstracta;

import java.util.Date;

/**
 *
 * @author Fernando
 */
public abstract class Persona {

	private String nombre;

	private Date fechaDeNacimiento;

	/**
	 * Los metodos abtractos no llevan cuerpo y al momento de heredar una clase
	 * abstracta este metodo debe ser sobreescrito
	 */
	public abstract void estudiar();

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

}
