/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.anotherpackage;


/**
 * Import de todas las clases que se encuentran en el paquete aeropuerto
 */
import clase4.interfaces.aeropuerto.*;

/**
 * Import de clase por clase que se encuentran en el paquete aeropuerto
 */
//import ar.com.educacionit.interfaces.AvionDePasajeros;
//import ar.com.educacionit.interfaces.AvionPrivado;


/**
 *
 * @author Fernando
 * 
 * Ambas formas de hacer import de una clase desde un paquete son validas.
 */
public class Programa {

    public static void main(String[] args) {
        AvionPrivado avionPrivado = new AvionPrivado();
        AvionDePasajeros avionPasajeros = new AvionDePasajeros();
        
        avionPrivado.aterrizar();
        avionPasajeros.aterrizar();
    }

}
