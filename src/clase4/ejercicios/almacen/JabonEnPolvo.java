package clase4.ejercicios.almacen;

public class JabonEnPolvo implements Producto{

	@Override
	public String tipoProducto() {
		return "Pasta";
	}

	@Override
	public String getNombreProducto() {
		return "Knor";
	}

	@Override
	public String getUbicacion() {
		return "A1B41";
	}

}
