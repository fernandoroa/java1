package clase4.ejercicios.almacen;

public interface Liquido {
	
	public int CantidadLitrosMaximo();
	
	public int CargarLitros(int litro);

}
