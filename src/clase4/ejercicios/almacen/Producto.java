package clase4.ejercicios.almacen;

public interface Producto {

	public String tipoProducto();
	
	public String getNombreProducto();
	
	public String getUbicacion();

}
