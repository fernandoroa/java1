package clase4.ejercicios.almacen;

public class ProgramaAlmacen {

	public static void main(String[] args) {
		
		
		Suavizante suavizante = new Suavizante("Querubin");
		JabonEnPolvo pasta = new JabonEnPolvo();
		/**
		 * Descomentar la linea para verificar que valor devuelve el metodo.
		 * El cual deberia ser el nombre del atributo nombre de suavizante
		 */
//		System.out.println(suavizante.getNombreProducto());

		Producto[] producto = new Producto[2];
		producto[0] = suavizante;
		producto[1] = pasta;
		
		for (Producto producto2 : producto) {
			System.out.println(producto2.getNombreProducto());
		}

	}

}
