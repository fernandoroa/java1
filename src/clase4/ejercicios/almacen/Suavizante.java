package clase4.ejercicios.almacen;

public class Suavizante implements Producto, Liquido {

	private int id;
	private int litro;
	private String ubicacion;
	private String nombre;

	public Suavizante(String nombre) {
		super();
		this.nombre = nombre;
	}

	@Override
	public int CantidadLitrosMaximo() {
		return 10;
	}

	@Override
	public int CargarLitros(int litro) {
		this.litro = litro;
		return this.litro;
	}

	@Override
	public String tipoProducto() {
		return "Suavizante";
	}

	@Override
	public String getNombreProducto() {
		return this.nombre;
	}

	@Override
	public String getUbicacion() {
		return ubicacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
