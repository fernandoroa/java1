package clase4.ejercicios.hacienda;

public abstract class Animal {

	private String color;
	private String peso;

	public abstract String getTipoAnimal();

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getPeso() {
		return peso;
	}

	public void setPeso(String peso) {
		this.peso = peso;
	}

}
