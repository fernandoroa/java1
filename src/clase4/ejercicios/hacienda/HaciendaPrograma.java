package clase4.ejercicios.hacienda;

public class HaciendaPrograma {

	public static void main(String[] args) {

		Animal[] animales = new Animal[2];
		Vaca vaca = new Vaca();
		vaca.setColor("blanca con negro");
		vaca.setPeso("200kg");

		Perro perro = new Perro();
		perro.setColor("marron");
		perro.setPeso("20kg");
		perro.setRaza("pincher");

		animales[0] = vaca;
		animales[1] = perro;

		for (Animal animal : animales) {
			if (animal instanceof Vaca) {
				System.out.println(animal.getTipoAnimal() + " - litros :" + ((Vaca) animal).cantidadLitros());
			} else {
				System.out.println(animal.getTipoAnimal());
			}
		}

	}

}
