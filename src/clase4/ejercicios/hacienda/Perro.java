package clase4.ejercicios.hacienda;

public class Perro extends Animal {

	private String raza;

	@Override
	public String getTipoAnimal() {
		return "Perro";
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

}
