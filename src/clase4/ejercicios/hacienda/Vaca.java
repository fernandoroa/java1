package clase4.ejercicios.hacienda;

public class Vaca extends Animal implements ProductorDeLeche {

	private int litros;
	private boolean tieneCrias;

	@Override
	public String getTipoAnimal() {
		return "Vaca";
	}

	@Override
	public int cantidadLitros() {
		return 30;
	}

	public int getLitros() {
		return litros;
	}

	public void setLitros(int litros) {
		this.litros = litros;
	}

	public boolean isTieneCrias() {
		return tieneCrias;
	}

	public void setTieneCrias(boolean tieneCrias) {
		this.tieneCrias = tieneCrias;
	}

}
