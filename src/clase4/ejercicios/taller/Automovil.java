package clase4.ejercicios.taller;

public abstract class Automovil {

	private String modelo;
	private String anio;
	private String marca;
	private int velocidadActual;
	protected boolean tieneNitro;

	public abstract String acelerar(int km);

	public abstract String frenar(int km);

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getVelocidadActual() {
		return velocidadActual;
	}

	public void setVelocidadActual(int velocidadActual) {
		this.velocidadActual = velocidadActual;
	}

}
