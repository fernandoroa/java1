package clase4.ejercicios.taller;

public class CarroCarrera extends Automovil {

	private String escuderia;
	private String tipoDeRuedas;

	@Override
	public String acelerar(int km) {
		return "Velocidad aumentada a : " + (getVelocidadActual() + km);
	}

	public String acelerar(int km, boolean nitro) {
		if(!nitro) {
			return acelerar(km);
		}
		return "Velocidad aumentada a : " + ((getVelocidadActual() + km) * 2);
	}

	@Override
	public String frenar(int km) {
		return "Velocidad reducida a : " + (getVelocidadActual() - km);
	}

	public String getEscuderia() {
		return escuderia;
	}

	public void setEscuderia(String escuderia) {
		this.escuderia = escuderia;
	}

	public String getTipoDeRuedas() {
		return tipoDeRuedas;
	}

	public void setTipoDeRuedas(String tipoDeRuedas) {
		this.tipoDeRuedas = tipoDeRuedas;
	}

}
