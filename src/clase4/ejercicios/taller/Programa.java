package clase4.ejercicios.taller;

public class Programa {
	
	public static void main(String[] args) {
		
		Sedan sedan = new Sedan();
		sedan.setVelocidadActual(0);
		sedan.acelerar(100);
		
		CarroCarrera carroCarrera = new CarroCarrera();
		carroCarrera.tieneNitro = true;
		System.out.println(carroCarrera.acelerar(100, carroCarrera.tieneNitro));
		
	}

}
