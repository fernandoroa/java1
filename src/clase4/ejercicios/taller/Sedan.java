package clase4.ejercicios.taller;

public class Sedan extends Automovil {

	private int cantidadPuertas;
	private boolean tieneAirBag;

	@Override
	public String frenar(int km) {
		return "Velocidad reducida a : " + (getVelocidadActual() - km);
	}

	@Override
	public String acelerar(int km) {
		return "Velocidad aumentada a : " + (getVelocidadActual() + km);
	}

	public int getCantidadPuertas() {
		return cantidadPuertas;
	}

	public void setCantidadPuertas(int cantidadPuertas) {
		this.cantidadPuertas = cantidadPuertas;
	}

	public boolean isTieneAirBag() {
		return tieneAirBag;
	}

	public void setTieneAirBag(boolean tieneAirBag) {
		this.tieneAirBag = tieneAirBag;
	}

}
