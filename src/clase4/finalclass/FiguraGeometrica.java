/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.finalclass;

/**
 *
 * @author Fernando
 */
public class FiguraGeometrica {

	public static final float PI = 3.14f;

	// Agregar modificador final
	public float obtenerPi() {
		return PI;
	}

}
