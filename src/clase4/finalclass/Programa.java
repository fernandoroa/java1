/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.finalclass;

/**
 *
 * @author Fernando
 */
public class Programa extends FiguraGeometrica {

	@Override
	public float obtenerPi() {
		return PI;
	}

	// Descomentar linea por motivos de estudios
	public void cambiarValorAPI() {
		// super.PI = 3.146666f;
	}

	public static void main(String[] args) {
		System.out.println("valor de PI :" + new FiguraGeometrica().obtenerPi());
	}

}
