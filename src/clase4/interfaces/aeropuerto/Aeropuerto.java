/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.interfaces.aeropuerto;

/**
 *
 * @author Fernando
 */
public class Aeropuerto {

    public void darPermisoDeAterrizar(Volador v) {

        // Aqui va el codigo de despejar pista y permitir aterrizar al Volador v.
        // En el programa principal se puede iterar sobre una lista de Voladores e ir dando
        // permiso de aterrizaje a cada volador, sin especificar que tipo especifico
        // de volador se trata
        v.aterrizar();

        if (v instanceof AvionPrivado) {
            AvionPrivado ap = (AvionPrivado) v;
            System.out.println("Aterrizo Licencia : " + ap.getLicencia());
        }

        if (v instanceof AvionDePasajeros) {
            AvionDePasajeros ap = (AvionDePasajeros) v;
            System.out.println("Aterrizo Aerolinea : " + ap.getAerolinea());

        }

        if (v instanceof Superman) {
            Superman ap = (Superman) v;
            System.out.println("Aterrizo Nombre : " + ap.getNombre());
        }

    }

}
