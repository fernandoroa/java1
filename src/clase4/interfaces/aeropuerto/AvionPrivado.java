/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.interfaces.aeropuerto;

/**
 *
 * @author Fernando
 */
public class AvionPrivado extends TransporteAereo implements Volador {

    public void aterrizar() {

        // Aqui va el codigo de modo que tiene de aterrizar un avion privado
    }

    private String licencia;

    public String getLicencia() {

        return licencia;

    }

    public void setLicencia(String lic) {

        this.licencia = lic;

    }

}
