/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase4.interfaces.aeropuerto;

/**
 *
 * @author Fernando
 */
public class Programa {

    public Programa() {

    }

    public static void main(String[] args) {

        // Una inicializacion que puede existir en cualquier lado del programa
        AvionPrivado v1 = new AvionPrivado();

        v1.setLicencia("L123456F");

        AvionDePasajeros v2 = new AvionDePasajeros();

        v2.setAerolinea("EducacionIT Lan");

        Superman v3 = new Superman();

        Aeropuerto aeropuerto1 = new Aeropuerto();

        aeropuerto1.darPermisoDeAterrizar(v1);

        aeropuerto1.darPermisoDeAterrizar(v2);

        aeropuerto1.darPermisoDeAterrizar(v3);

    }

}
