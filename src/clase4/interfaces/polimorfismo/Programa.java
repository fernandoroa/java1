package clase4.interfaces.polimorfismo;

import java.util.Scanner;

public class Programa {

    static ServiceGeneric serviceGeneric;

    public static void main(String[] args) {
        int serviceID = 0;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("SERVICIOS....");
            System.out.println("1.- AltaFirmante");
            System.out.println("2.- Paquete");
            System.out.println("3.- SuperCuenta");

            System.out.print("Indique que interface quiere invocar: ");
            serviceID = scanner.nextInt();
            System.out.println("\n################################");
            serviceGeneric = new Programa().filter(serviceID);
            if (serviceGeneric != null) {
                System.out.println("RESULTADO :");
                serviceGeneric.alta(serviceGeneric.getClass().toString());
            }
            System.out.println("\n");
            System.out.println("#################################");
            System.out.println("\n");

        } while (serviceID != 0);
    }

    public ServiceGeneric filter(int serviceID) {
        switch (serviceID) {
            case 1:
                serviceGeneric = new AltaFirmanteServiceImpl();
                break;
            case 2:
                serviceGeneric = new PaqueteServiceImpl();
                break;
            case 3:
                serviceGeneric = new SuperCuentaServiceImpl();
                break;
            default:
                System.out.println("No encuentro el servicio");
                serviceGeneric = null;
                break;
        }
        return serviceGeneric;
    }

}
