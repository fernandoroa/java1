package clase4.interfaces.polimorfismo;

public class SuperCuentaServiceImpl implements SuperCuentaService , ServiceGeneric{

	@Override
	public void altaSuperCuenta() {

	}

	@Override
	public void alta(String service) {
		System.out.println("Soy -> " + service);

	}

}
