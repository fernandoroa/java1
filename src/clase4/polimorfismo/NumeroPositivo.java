package clase4.polimorfismo;

public class NumeroPositivo extends Numero {

	public NumeroPositivo() {
		numero = 0;
	}

	public void Proximo() {
		numero = numero + 1;
	}

}
