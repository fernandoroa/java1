package clase4.polimorfismo;

public class Programa {

    public static void main(String[] args) {

//        Numero Numero = new Numero();
//        Numero.numero = 10;

        Numero positivo = new NumeroPositivo();

        Numero negativo = new NumeroNegativo();

        System.out.println("numero positivo inicial:" + positivo);

        System.out.println("numero negativo inicial:" + negativo);

        positivo.Proximo();

        negativo.Proximo();

        System.out.println("numero positivo proximo:" + positivo);

        System.out.println("numero negativo proximo:" + negativo);

        positivo.Proximo();

        negativo.Proximo();

        System.out.println("numero positivo proximo:" + positivo);

        System.out.println("numero negativo proximo:" + negativo);
    }

}
