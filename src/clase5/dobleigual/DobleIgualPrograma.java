package clase5.dobleigual;

public class DobleIgualPrograma {

	public static void main(String[] args) {
		String s1 = new String("Hola");
		String s2 = s1;
		String s3 = new String("Hola");

		if (s1 == s2) {
			System.out.println("S1 y S2 son iguales");
		}

		/**
		 * Esta validacion consiste en validar las diferencias que hay en entre punteros
		 * de memoria y el contenido del mismo
		 */
		if (s1 == s3) {
			System.out.println("S1 y S3 son iguales");
		} else {
			System.out.println("S1 y S3 son diferentes");
		}

	}

}
