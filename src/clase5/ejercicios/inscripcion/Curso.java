/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.ejercicios.inscripcion;

/**
 *
 * @author Fernando
 */
public class Curso {

    private int id;
    private String descripcion;
    private String horario;

    public Curso() {
    }

    public Curso(int id, String descripcion, String horario) {
        this.id = id;
        this.descripcion = descripcion;
        this.horario = horario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

}
