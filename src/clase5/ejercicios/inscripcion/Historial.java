package clase5.ejercicios.inscripcion;

import java.util.Date;

public class Historial {
	private Date fechaAlta;
	private Date fechBaja;
	private String usuarioAlta;
	private String usuarioBaja;

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Date getFechBaja() {
		return fechBaja;
	}

	public void setFechBaja(Date fechBaja) {
		this.fechBaja = fechBaja;
	}

	public String getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public String getUsuarioBaja() {
		return usuarioBaja;
	}

	public void setUsuarioBaja(String usuarioBaja) {
		this.usuarioBaja = usuarioBaja;
	}

}
