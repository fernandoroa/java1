/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.ejercicios.inscripcion;

import java.util.Date;

/**
 *
 * @author Fernando
 */
public class InscripcionPrograma {

	public static void main(String[] args) {
		Inscripcion inscripcion = new Inscripcion();
		inscripcion.setCurso(new Curso(1, "Java Standard", "Lun-Vie"));
		Estudiante estudiante = new Estudiante(0, "Pedro", "Carrasco", 18, 120);
		estudiante.setFechaAlta(new Date());
		estudiante.setUsuarioAlta("Fernand");
		inscripcion.setEstudiante(estudiante);

		System.out.println("contenido inscripcion : " + inscripcion.getEstudiante().toString() + " Usuario Alta :"
				+ inscripcion.getEstudiante().getFechaAlta());

	}

}
