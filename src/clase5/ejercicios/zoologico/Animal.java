/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.ejercicios.zoologico;

/**
 *
 * @author fmelendez
 */
public class Animal {

    private int Id;
    private String tipoAnimal;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getTipoAnimal() {
        return tipoAnimal;
    }

    public void setTipoAnimal(String tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }

    public void comer(Animal animal) {
        System.out.println("Comer como un :" + animal.getTipoAnimal());
    }

    public void dormir(Animal animal) {
        System.out.println("Dormir como un :" + animal.getTipoAnimal());
    }

    public void correr(Animal animal) {
        System.out.println("Correr como un :" + animal.getTipoAnimal());
    }

}
