/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.ejercicios.zoologico;

/**
 *
 * @author fmelendez
 */
public class Panda extends Animal {

   private String nombre;
   private int peso;
   private int tamanio; 
   private String nombrePadre;
   private String nombreMadre;

    public Panda() {
    }

    public Panda(String nombre, int peso, int tamanio, String nombrePadre, String nombreMadre) {
        this.nombre = nombre;
        this.peso = peso;
        this.tamanio = tamanio;
        this.nombrePadre = nombrePadre;
        this.nombreMadre = nombreMadre;
    }
   
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public String getNombrePadre() {
        return nombrePadre;
    }

    public void setNombrePadre(String nombrePadre) {
        this.nombrePadre = nombrePadre;
    }

    public String getNombreMadre() {
        return nombreMadre;
    }

    public void setNombreMadre(String nombreMadre) {
        this.nombreMadre = nombreMadre;
    }

    @Override
    public String toString() {
        return "Nombre : "+getNombre()+ " peso: "+getPeso() + " tamanio : "+getTamanio() ; //To change body of generated methods, choose Tools | Templates.
    }

}
