/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.ejercicios.zoologico;

/**
 *
 * @author fmelendez
 */
public class ZoologicoPrograma {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {

		Animal animal = new Panda();
		animal.setId(1);
		animal.setTipoAnimal("panda");
		animal.comer(animal);
		animal.correr(animal);
		animal.dormir(animal);

		Panda panda = (Panda) animal;
		panda.setNombre("poncho");
		panda.setPeso(300);
		panda.setTamanio(120);
		panda.setNombreMadre("lola");
		panda.setNombrePadre("marco");

		System.out.println(panda.toString());
	}

}
