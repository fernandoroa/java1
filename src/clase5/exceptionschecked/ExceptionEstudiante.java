/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.exceptionschecked;

/**
 *
 * @author fernando.melendez
 */
public class ExceptionEstudiante extends Exception{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codError;
    private String mensaje;

    public ExceptionEstudiante() {
    }

    public ExceptionEstudiante(String codError, String mensaje) {
        this.codError = codError;
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return "ExectionEstudiante :"+  getMensaje();
    }
    
    
    
}
