/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase5.exceptionschecked;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author fernando.melendez
 */
public class FileReaderPrograma {
    
    public static void main(String[] args) throws ExceptionEstudiante {
        try {
            FileReader programa = new FileReader(new File("prueba1.txt"));
        } catch (FileNotFoundException ex) {
           throw new ExceptionEstudiante("2","Fallo al buscar el archivo "+ex);
        }finally{
            System.out.println("se libera el archivo");
        } 
    }
    
}
