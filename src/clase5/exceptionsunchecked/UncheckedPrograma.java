package clase5.exceptionsunchecked;

public class UncheckedPrograma {

	public static void main(String[] args) {
		// Este valor se suponia que no podia ser Cero,
		// pero en algun momento se convirtio en Cero
		int unValor = 0;
		try {
			int resultado = 10 / unValor;
		} catch (ArithmeticException ae) {
			System.out.println(ae.getMessage());
		} finally {
			System.out.println("Error en el calculo");
		}
	}

}
