package clase5.wrappers;

public class WrappersPrograma {

	public static void main(String[] args) {
		String numeroString = "10";

		Integer numeroInt = Integer.parseInt(numeroString);
		Integer numeroInt2 = Integer.valueOf(numeroString);

		System.out.println("numeroInt :" + numeroInt + " numeroInt2 :" + numeroInt2);

		Float miPuntoFlotante = new Float(45.67);
		Float miOtroPuntoFlotante = Float.valueOf("45.67");

		System.out.println("miPuntoFlotante :" + miPuntoFlotante + " miOtroPuntoFlotante :" + miOtroPuntoFlotante);

		Integer comparar = Integer.min(12, 20);
		System.out.println(comparar);

		tienesObject(null);
		/**
		 * Descomentar la siguiente linea para validar la diferencia entre el uso de
		 * primitivos y wrappers
		 */
		// tienesBoolean(null);

	}

	private static void tienesBoolean(boolean flag) {
		System.out.println(flag);
	}

	private static void tienesObject(Integer flag) {
		System.out.println(flag);
	}
}
