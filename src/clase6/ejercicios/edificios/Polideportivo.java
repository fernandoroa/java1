/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.edificios;

/**
 *
 * @author Fernando
 */
public class Polideportivo implements InInstalacionDeportiva, IEdificio {

    private double largo;
    private double ancho;
    private String nombre;
    private String tipoInstalacion;

    public Polideportivo() {
        this.largo = 0;
        this.ancho = 0;
        this.nombre = "";
        this.tipoInstalacion = "";
    }

    public Polideportivo(double largo, double ancho, String nombre, String tipoInstalacion) {
        this.largo = largo;
        this.ancho = ancho;
        this.nombre = nombre;
        this.tipoInstalacion = tipoInstalacion;
    }

    @Override
    public int getTipoInstalacion() {
        switch (tipoInstalacion) {
            case "Techado":
                return 1;
            case "Abierto":
                return 2;
            default:
                return 3;
        }
    }

    @Override
    public double getSuperficieEdificio() {
        return ancho * largo;
    }

    @Override
    public String toString() {
        return "\"Polideportivo " + getNombre() + "\": Posee una superficie de " + getSuperficieEdificio() + " metros, y es de tipo " + tipoInstalacion;

    }

    public double getLargo() {
        return largo;
    }

    public void setLargo(double largo) {
        this.largo = largo;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTipoInstalacion(String tipoInstalacion) {
        this.tipoInstalacion = tipoInstalacion;
    }

}
