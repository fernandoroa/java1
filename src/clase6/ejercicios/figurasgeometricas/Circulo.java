/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public class Circulo extends FiguraGeometrica {

    private double radio;

    public Circulo(double radio) {
        this.radio = radio;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    @Override
    public double area() {
        //Se puede usar Math.PI
        return PI * Math.pow(getRadio(), 2);
    }

    @Override
    public String toString() {
        return "Circulo"; //To change body of generated methods, choose Tools | Templates.
    }

}
