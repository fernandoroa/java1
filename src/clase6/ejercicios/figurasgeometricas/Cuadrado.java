/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public class Cuadrado extends FiguraGeometrica {

    // area=a*a;
    private int lado;
    private int resultado;

    public Cuadrado(int lado) {
        this.lado = lado;
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    @Override
    public double area() {
        return getLado() * getLado();
    }
    
      @Override
    public String toString() {
        return "Cuadrado"; //To change body of generated methods, choose Tools | Templates.
    }

}
