/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public abstract class FiguraGeometrica {

    public final float PI = 3.14F;

    public abstract double area();

    public void showResultado(double resultado, String nombreFigura) {
        System.out.println("Resultado area: " + resultado + " de un " + nombreFigura);
    }
}
