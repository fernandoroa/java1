/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public class Programa {

    public static void main(String[] args) {

        FiguraGeometrica figuraGeometrica = new Triangulo(5, 8);
        /**
         * El calculo de area lo realiza especificamente la clase hija
         */
        figuraGeometrica.showResultado(figuraGeometrica.area(), figuraGeometrica.toString());

    }

}
