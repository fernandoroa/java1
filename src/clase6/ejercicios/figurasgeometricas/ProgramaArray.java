/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public class ProgramaArray {

    public static void main(String[] args) {
        //Arreglo de FigurasGeometricas
        FiguraGeometrica[] figurasGeometricas = new FiguraGeometrica[3];
        figurasGeometricas[0] = new Triangulo(2, 8);
        figurasGeometricas[1] = new Cuadrado(2);
        figurasGeometricas[2] = new Circulo(5);

        for (FiguraGeometrica figurasGeometrica : figurasGeometricas) {
            FiguraGeometrica figuraGeometrica = figurasGeometrica;
            /**
             * figuraGeometrica.toString() invoca el metodo toString de su
             * referencia de clase, esto se puede ver en tiempo de ejecucion
             */
            figuraGeometrica.showResultado(figurasGeometrica.area(), figuraGeometrica.toString());
        }

    }

}
