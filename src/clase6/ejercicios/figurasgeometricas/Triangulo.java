/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.figurasgeometricas;

/**
 *
 * @author Fernando
 */
public class Triangulo extends FiguraGeometrica {

	private int altura;
	private int base;
	private double resultado;

	public Triangulo(int altura, int base) {
		this.altura = altura;
		this.base = base;
	}

	public int getAltura() {
		return altura;
	}

	public void setAltura(int altura) {
		this.altura = altura;
	}

	public int getBase() {
		return base;
	}

	public void setBase(int base) {
		this.base = base;
	}

	public double getResultado() {
		return resultado;
	}

	public void setResultado(double resultado) {
		this.resultado = resultado;
	}

	@Override
	public double area() {
		return (double) (getBase() * getAltura()) / 2;
	}

	@Override
	public String toString() {
		return "Triangulo"; // To change body of generated methods, choose Tools | Templates.
	}
}
