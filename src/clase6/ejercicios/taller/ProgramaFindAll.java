package clase6.ejercicios.taller;

import java.util.List;

import clase6.ejercicios.taller.dao.VehiculoDao;
import clase6.ejercicios.taller.model.Vehiculo;

public class ProgramaFindAll {

	public static void main(String[] args) throws Exception {
		VehiculoDao vehiculoDao = new VehiculoDao();
		List<Vehiculo> vehiculos = vehiculoDao.findAll();
		vehiculos.stream().forEach(action -> System.out.println(action.toString()));
	}
}
