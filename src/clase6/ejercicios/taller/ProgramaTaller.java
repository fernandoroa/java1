package clase6.ejercicios.taller;

import java.sql.Date;

import clase6.ejercicios.taller.dao.FacturaDao;
import clase6.ejercicios.taller.dao.PersonaDao;
import clase6.ejercicios.taller.dao.VehiculoDao;
import clase6.ejercicios.taller.model.Factura;
import clase6.ejercicios.taller.model.Persona;
import clase6.ejercicios.taller.model.Vehiculo;

public class ProgramaTaller {

	public static void main(String[] args) throws Exception {
		VehiculoDao vehiculoRepository = new VehiculoDao();
		PersonaDao personaRepository = new PersonaDao();
		FacturaDao facturaRepository = new FacturaDao();

		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setId(0);
		vehiculo.setMarca("Renault");
		vehiculo.setModelo("Clio");
		vehiculo.setMatricula("AYB-010");
		vehiculo.setAnio("2018");
		vehiculo.setFechaAlta(new Date(new java.util.Date().getTime()));
		Vehiculo vehiculoCreate = vehiculoRepository.create(vehiculo);

		Persona persona = new Persona();
		persona.setId(0);
		persona.setNombre("pedro");
		persona.setApellido("perez");
		persona.setDocumento(18456987);
		persona.setSexo("M");
		persona.setEdad(31);
		persona.setEmail("corre@correo.com");
		persona.alta();
		persona.setActivo(1);
		Persona personaCreate = personaRepository.create(persona);

		Factura factura = new Factura();
		factura.setId(0);
		factura.setNroFactura(Integer.valueOf((int) Math.floor(1000 + Math.random() * 9000)));
		factura.setFechaFactura(new Date(new java.util.Date().getTime()));
		factura.setMontoTotal(1000);
		factura.setIdPersona(personaCreate.getId());
		factura.setIdVehiculo(vehiculoCreate.getId());
		factura.alta();
		Factura facturaCreate = facturaRepository.create(factura);

		System.out
				.println("Datos Factura: " + facturaCreate.getId() + " - NroFactura: " + facturaCreate.getNroFactura());
	}

}
