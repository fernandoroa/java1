package clase6.ejercicios.taller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import clase6.ejercicios.taller.model.Factura;
import clase6.ejercicios.taller.repository.AdministradorDeConexiones;

public class FacturaDao {

	public Factura findById(Integer id) throws Exception {
		Factura factura = new Factura();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM factura WHERE id = " + id);
		Connection con = AdministradorDeConexiones.obtenerConexion();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql.toString());
		if (rs.next()) {
			factura.setNroFactura(rs.getInt("nrofactura"));
			factura.setFechaFactura(rs.getDate("fecha_factura"));
			factura.setMontoTotal(rs.getInt("monto_total"));
			factura.setIdPersona(rs.getInt("id_Persona"));
			factura.setIdVehiculo(rs.getInt("id_Vehiculo"));
			factura.setFechaAlta(rs.getDate("fecha_alta"));
			factura.setFechaBaja(rs.getDate("fecha_baja"));
			factura.setUsuarioAlta(rs.getString("usuario_alta"));
			factura.setUsuarioBaja(rs.getString("usuario_baja"));
		}
		rs.close();
		st.close();
		con.close();
		return factura;
	}

	public int delete(Integer id) throws Exception {
		String sql = "DELETE FROM vehiculo WHERE id =" + id;
		int resultado = 0;
		Statement stmt;
		try {
			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sql);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, e);
		}

		return resultado;
	}

	public Factura create(Factura factura) {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"INSERT INTO factura (id,nroFactura,fecha_factura,monto_total,id_persona,id_vehiculo,fecha_alta,fecha_baja,usuario_alta,usuario_baja,activo)");
			sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?)");
			Connection con = AdministradorDeConexiones.obtenerConexion();
			PreparedStatement stmt = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, 0);
			stmt.setInt(2, factura.getNroFactura());
			stmt.setDate(3, factura.getFechaFactura());
			stmt.setInt(4, factura.getMontoTotal());
			stmt.setInt(5, factura.getIdPersona());
			stmt.setInt(6, factura.getIdVehiculo());
			stmt.setDate(7, factura.getFechaAlta());
			stmt.setDate(8, null);
			stmt.setString(9, factura.getUsuarioAlta());
			stmt.setString(10, null);
			stmt.setInt(11, 1);
			stmt.executeUpdate();
			/**
			 * Se obtiene el Id con el cual fue almacenado el registro
			 */
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			factura.setId(rs.getInt(1));

			/**
			 * Se cierra el ResultSet el Statement y la conexion para liberar recursos
			 */
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception e) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, e);
		}

		return factura;
	}

	public int update(Factura f) throws Exception {
		int resultado = 0;
		Statement stmt;
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("UPDATE factura SET ").append(" nrofactura = '").append(f.getNroFactura()).append("',")
					.append(" monto_total = ").append(f.getMontoTotal()).append(",").append(" id_Persona = ")
					.append(f.getIdPersona()).append(",").append(" id_Vehiculo = ").append(f.getIdVehiculo())
					.append(" ").append("wHERE factura.id = ").append(f.getId());

			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sbQuery.toString());
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, e);
		}

		return resultado;
	}

}
