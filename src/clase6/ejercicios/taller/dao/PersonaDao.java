package clase6.ejercicios.taller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import clase6.ejercicios.taller.model.Persona;
import clase6.ejercicios.taller.repository.AdministradorDeConexiones;

public class PersonaDao {

	public Persona findById(Integer id) throws Exception {
		Persona persona = new Persona();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT * FROM persona WHERE id = " + id);
		Connection con = AdministradorDeConexiones.obtenerConexion();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql.toString());
		if (rs.next()) {
			persona.setNombre(rs.getString("nombre"));
			persona.setApellido(rs.getString("apellido"));
			persona.setDocumento(rs.getInt("documento"));
			persona.setSexo(rs.getString("sexo"));
			persona.setEdad(rs.getInt("edad"));
			persona.setEmail(rs.getString("email"));
		}
		rs.close();
		st.close();
		con.close();
		return persona;

	}

	public int delete(Integer id) throws Exception {
		String sql = "DELETE FROM persona WHERE id =" + id;
		int resultado = 0;
		Statement stmt;
		try {
			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sql);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, e);
		}

		return resultado;
	}

	public Persona create(Persona persona) throws Exception {
		try {

			StringBuffer sql = new StringBuffer();
			sql.append(
					"INSERT INTO persona (id,nombre,apellido,documento,sexo,edad,email,fecha_alta,fecha_baja,usuario_alta,usuario_baja,activo)");
			sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
			Connection con = AdministradorDeConexiones.obtenerConexion();
			PreparedStatement stmt = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, 0);
			stmt.setString(2, persona.getNombre());
			stmt.setString(3, persona.getApellido());
			stmt.setInt(4, persona.getDocumento());
			stmt.setString(5, persona.getSexo());
			stmt.setInt(6, persona.getEdad());
			stmt.setString(7, persona.getEmail());
			stmt.setDate(8, persona.getFechaAlta());
			stmt.setDate(9, null); // fecha baja
			stmt.setString(10, "user01"); // fecha baja
			stmt.setString(11, null); // fecha baja
			stmt.setInt(12, 1);
			stmt.executeUpdate();

			/**
			 * Se obtiene el Id con el cual fue almacenado el registro
			 */
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			persona.setId(rs.getInt(1));

			/**
			 * Se cierra el ResultSet el Statement y la conexion para liberar recursos
			 */
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(PersonaDao.class.getName()).log(Level.SEVERE, null, ex);
		}

		return persona;
	}

	public int update(Persona p) throws Exception {
		int resultado = 0;
		Statement stmt;
		try {
			StringBuilder sbQuery = new StringBuilder();
			sbQuery.append("UPDATE persona SET ")
			.append(" nombre  = '").append(p.getNombre()).append("',")
			.append(" apellido  = '").append(p.getApellido()).append("',")
			.append(" documento  = ").append(p.getDocumento()).append(",")
			.append(" sexo = '").append(p.getSexo()).append("',")
			.append(" edad = ").append(p.getEdad()).append(",")
			.append(" email = ").append(p.getEmail())
			.append(" ").append("wHERE persona.id = ").append(p.getId());

			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sbQuery.toString());
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(FacturaDao.class.getName()).log(Level.SEVERE, null, e);
		}

		return resultado;
	}

	public int updateActivo(Persona t) {
		return 0;
	}
}
