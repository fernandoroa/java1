package clase6.ejercicios.taller.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import clase6.ejercicios.taller.repository.AdministradorDeConexiones;
import clase6.ejercicios.taller.model.Vehiculo;

public class VehiculoDao {
	
	public Vehiculo findById(Integer id) throws Exception {
		Vehiculo vehiculo = new Vehiculo();
		try {
			Connection con = AdministradorDeConexiones.obtenerConexion();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from vehiculo where id = " + id);
			while (rs.next()) {
				vehiculo.setId(rs.getInt(1));
				vehiculo.setMarca(rs.getString(2));
			}
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(VehiculoDao.class.getName()).log(Level.SEVERE, null, ex);
		}

		return vehiculo;
	}

	public int delete(Integer id) throws Exception {
		String sql = "DELETE FROM vehiculo WHERE id =" + id;
		int resultado = 0;
		Statement stmt;
		try {
			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sql);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(VehiculoDao.class.getName()).log(Level.SEVERE, null, e);
		}
		
		return resultado;
	}

	public int update(Vehiculo vehiculo) throws Exception {
		int resultado = 0;
		Statement stmt;
		try {
			String sql = "UPDATE vehiculo SET marca = '"+vehiculo.getMarca()+"', modelo = '"+vehiculo.getModelo()+"', matricula = '"+vehiculo.getMatricula()+"',"
					+ " anio = '"+vehiculo.getAnio()+"', activo = "+vehiculo.getActivo()+" WHERE id = "+vehiculo.getId();
			Connection con = AdministradorDeConexiones.obtenerConexion();
			stmt = con.createStatement();
			resultado = stmt.executeUpdate(sql);
			stmt.close();
			con.close();
		} catch (SQLException e) {
			Logger.getLogger(VehiculoDao.class.getName()).log(Level.SEVERE, null, e);
		}
		
		return resultado;
	}

	public Vehiculo create(Vehiculo vehiculo) throws Exception {
		try {
			String sql = "INSERT INTO vehiculo (id,marca,modelo,matricula,anio,fecha_alta) VALUES(?,?,?,?,?,?)";
			Connection con = AdministradorDeConexiones.obtenerConexion();
			PreparedStatement stmt =con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, 0);
			stmt.setString(2, vehiculo.getMarca());
			stmt.setString(3, vehiculo.getModelo());
			stmt.setString(4, vehiculo.getMatricula());
			stmt.setString(5, vehiculo.getAnio());
			stmt.setDate(6, vehiculo.getFechaAlta());
			stmt.executeUpdate();

			/**
			 * Se obtiene el Id con el cual fue almacenado el registro
			 */
			ResultSet rs = stmt.getGeneratedKeys();
			rs.next();
			vehiculo.setId(rs.getInt(1));

			/**
			 * Se cierra el ResultSet el Statement y 
			 * la conexion para liberar recursos
			 */
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(VehiculoDao.class.getName()).log(Level.SEVERE, null, ex);
		}

		return vehiculo;
	}
	
	
	
	public ArrayList<Vehiculo> findAll() throws Exception{
		ArrayList<Vehiculo>  vehiculos = new ArrayList<>();
		try {
			Connection con = AdministradorDeConexiones.obtenerConexion();
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from vehiculo");
			while (rs.next()) {
				vehiculos.add(new Vehiculo(rs.getInt("id"),
						rs.getString("marca"),
						rs.getString("modelo"),
						rs.getString("matricula"),
						rs.getString("anio"), 
						rs.getInt("activo")));
			}
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(VehiculoDao.class.getName()).log(Level.SEVERE, null, ex);
		}

		return vehiculos;
	}
}
