/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.taller.model;

import java.sql.Date;

/**
 *
 * @author Fernando
 */
public class Historial {

    private Date FechaAlta;
    private Date FechaBaja;
    private String UsuarioAlta;
    private String UsuarioBaja;

    public Date getFechaAlta() {
        return FechaAlta;
    }

    public void setFechaAlta(Date FechaAlta) {
        this.FechaAlta = FechaAlta;
    }

    public Date getFechaBaja() {
        return FechaBaja;
    }

    public void setFechaBaja(Date FechaBaja) {
        this.FechaBaja = FechaBaja;
    }

    public String getUsuarioAlta() {
        return UsuarioAlta;
    }

    public void setUsuarioAlta(String UsuarioAlta) {
        this.UsuarioAlta = UsuarioAlta;
    }

    public String getUsuarioBaja() {
        return UsuarioBaja;
    }

    public void setUsuarioBaja(String UsuarioBaja) {
        this.UsuarioBaja = UsuarioBaja;
    }

    
    protected void alta() {
    	setFechaAlta(new Date(new java.util.Date().getTime()));
    	setUsuarioAlta("user01");
    }
    
    
}
