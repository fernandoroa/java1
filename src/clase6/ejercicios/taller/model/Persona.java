/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.taller.model;

/**
 *
 * @author Fernando
 */
public class Persona extends Historial {

	private int id;
	private String Nombre;
	private String Apellido;
	private int Documento;
	private String Sexo;
	private int Edad;
	private String email;
	private int activo;

	public Persona() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Persona(int id, String sexo) {
		super();
		this.id = id;
		Sexo = sexo;
	}

	public Persona(int id, String sexo, String nombre) {
		super();
		this.id = id;
		Nombre = nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}

	public String getApellido() {
		return Apellido;
	}

	public void setApellido(String Apellido) {
		this.Apellido = Apellido;
	}

	public int getDocumento() {
		return Documento;
	}

	public void setDocumento(int Documento) {
		this.Documento = Documento;
	}

	public String getSexo() {
		return Sexo;
	}

	public void setSexo(String Sexo) {
		this.Sexo = Sexo;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int Edad) {
		this.Edad = Edad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	public void alta() {
		super.alta();
	}

	@Override
	public String toString() {
		return getNombre();
	}

}
