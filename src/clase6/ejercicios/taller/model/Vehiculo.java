/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase6.ejercicios.taller.model;

/**
 *
 * @author Fernando
 */
public class Vehiculo extends Historial {

	private int id;
	private String Marca;
	private String Modelo;
	private String matricula;
	private String anio;
	private int activo;

	public Vehiculo() {
		super();
	}

	public Vehiculo(int id, String marca, String modelo, String matricula, String anio, int activo) {
		super();
		this.id = id;
		Marca = marca;
		Modelo = modelo;
		this.matricula = matricula;
		this.anio = anio;
		this.activo = activo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return Marca;
	}

	public void setMarca(String Marca) {
		this.Marca = Marca;
	}

	public String getModelo() {
		return Modelo;
	}

	public void setModelo(String Modelo) {
		this.Modelo = Modelo;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}
	
	@Override
	public String toString() {
		return "Vehiculo : "+getId() +" Marca: "+getMarca() + " Matricula : "+getMatricula();
	}

}
