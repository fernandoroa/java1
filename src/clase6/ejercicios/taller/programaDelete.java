package clase6.ejercicios.taller;
import clase6.ejercicios.taller.dao.VehiculoDao;

public class programaDelete {
	
	public static void main(String[] args) throws Exception {
		VehiculoDao vehiculoRepository = new VehiculoDao();
		int resultado = vehiculoRepository.delete(2);
		System.out.println(resultado == 0 ? "No se pudo realizar el borrado": "Dato Eliminado");
	}

}
