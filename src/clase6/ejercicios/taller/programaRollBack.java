package clase6.ejercicios.taller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import clase6.ejercicios.taller.model.Persona;
import clase6.ejercicios.taller.repository.AdministradorDeConexiones;

public class programaRollBack {

	public static void main(String[] args) throws Exception {

		Connection conn = AdministradorDeConexiones.obtenerConexion();
		try {
			conn.setAutoCommit(false);
			Persona persona = new Persona(1, "F");
			Persona persona2 = new Persona();
			persona2.setId(0);
			persona2.setSexo("MD");

			String sql = "INSERT INTO persona (id,sexo) VALUES(?,?)";
			PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt.setInt(1, 0);
			stmt.setString(2, persona.getSexo());
			stmt.executeUpdate();

			PreparedStatement stmt2 = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			stmt2.setInt(1, 0);
			stmt2.setString(2, persona2.getSexo());
			stmt2.executeUpdate();
			

			conn.commit();
		} catch (SQLException e) {
			conn.rollback();
			e.printStackTrace();
		} finally {
			conn.close();
		}

	}
}
