package clase6.ejercicios.taller;

import clase6.ejercicios.taller.dao.VehiculoDao;
import clase6.ejercicios.taller.model.Vehiculo;

public class programaUpdate {

	public static void main(String[] args) throws Exception {
		VehiculoDao vehiculoRepository = new VehiculoDao();
		Vehiculo vehiculo = new Vehiculo();
		vehiculo.setId(1);
		vehiculo.setMarca("Toyota");
		vehiculo.setModelo("Hilux");
		vehiculo.setMatricula("ABK-018");
		vehiculo.setAnio("2012");
		vehiculo.setActivo(1);
		int resultado = vehiculoRepository.update(vehiculo);
		System.out.println(resultado == 1 ? "El registro ha sido modificado" : "El registro no pudo ser modificado");

	}
}
