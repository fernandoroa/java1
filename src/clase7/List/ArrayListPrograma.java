/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.List;

import java.util.ArrayList;

/**
 *
 * @author FernandoO
 */
public class ArrayListPrograma {

	public static void main(String[] args) {
		ArrayList<Book> arrayList = new ArrayList<Book>();
		arrayList.add(new Book(1, "Guerra 2", "Sin autor"));
		arrayList.add(new Book(2, "Guerra 3", "Sin autor"));
		arrayList.add(new Book(3, "Guerra 1", "Sin autor"));

		arrayList.remove(1);
		System.out.println(arrayList);

	}

}
