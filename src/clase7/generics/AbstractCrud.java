/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.generics;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author fernando.melendez
 * @param <T>
 */
public abstract class AbstractCrud<T> {

    List<T> elements;

    public abstract void addElement(T t);
    
    public abstract T findElement(Integer id);
    
    public abstract void deleteElement(Integer id);
    

    public void init() {
        elements = new ArrayList<>();
    }

    public List<T> getElements() {
        return elements;
    }

}
