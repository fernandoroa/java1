/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.generics;

/**
 *
 * @author fernando.melendezF
 */
public class BooksCrudPrograma {

    public static void main(String[] args) {
        BooksService crudBooks = new BooksService();
        crudBooks.init();
        crudBooks.addElement(new Book(1, "Esperanza", "Sin Nombre"));
        crudBooks.addElement(new Book(2, "Pinocho", "Pepeto"));
        crudBooks.addElement(new Book(3, "Java a Fondo", "AlfaOmega"));
        crudBooks.getElements().stream().forEach(book -> System.out.println("titulo: " + book.getTitulo()));

        Book book = crudBooks.findElement(2);
        System.out.println("" + book.getTitulo());

        System.err.println("############# Delete elements ##########");
        crudBooks.deleteElement(2);
        crudBooks.getElements().stream().forEach(b -> System.out.println("titulo: " + b.getTitulo()));

    }

}
