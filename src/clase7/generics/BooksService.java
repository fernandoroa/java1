/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.generics;



/**
 *
 * @author fernando.melendez
 */
public class BooksService extends AbstractCrud<Book> {

	@Override
	public void addElement(Book t) {
		this.elements.add(t);
	}

	@Override
	public Book findElement(Integer id) {
		Book bookReturn = null;
		for (Book element : elements) {
			if (element.getId() == id) {
				bookReturn = element;
				break;
			}
		}

//        for (Book element : elements) {
//            if(element.getId()== id){
//                return element;
//            }
//        }
		return bookReturn;

//       return getElements().stream().filter(book -> book.getId() == id).findAny().orElse(null);
	}

	@Override
	public void deleteElement(Integer id) {
		Book book = findElement(id);
		getElements().remove(book);
	}

}
