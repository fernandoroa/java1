/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.set;

import java.util.HashSet;

/**
 *
 * @author Fernando
 */
public class HashSetPrograma {

    public static void main(String[] args) {
        HashSet<String> hashSet = new HashSet<String>();

        hashSet.add("Carlos");
        hashSet.add("Maria");

        System.out.println(hashSet);

        hashSet.remove("Maria");

        System.out.println(hashSet);
    }

}
