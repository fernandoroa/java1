/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.set;

import java.util.LinkedHashSet;

/**
 *
 * @author Fernando
 */
public class LinkedHashSetPrograma {

    public static void main(String[] args) {
        LinkedHashSet<Book> books = new LinkedHashSet<>();
        books.add(new Book(1, "Guerra 2", "Sin autor"));
        books.add(new Book(3, "Suenios", "Sin Autor"));
        books.add(new Book(2, "Sin nombre", "Sin Autor"));
        books.stream().forEach(book -> System.out.println("Id: " + book.getId() + " Titulo: " + book.getTitulo()));
    }

}
