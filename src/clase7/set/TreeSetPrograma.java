/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.set;

import java.util.LinkedHashSet;
import java.util.TreeSet;

/**
 *
 * @author Fernando
 */
public class TreeSetPrograma {

    public static void main(String[] args) {
        TreeSet<String> treeSet = new TreeSet<String>();

        treeSet.add("Zanahoria");
        treeSet.add("Tomate");
        treeSet.add("Arroz");

        System.out.println(treeSet);
        treeSet.remove("Tomate");
        System.out.println(treeSet);
    }

}
