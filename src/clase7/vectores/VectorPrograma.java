/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase7.vectores;

import java.util.Vector;

import clase7.set.Book;

/**
 *
 * @author fernando.melendez
 */
public class VectorPrograma {

	public static void main(String[] args) {
		Vector<Book> books = new Vector();
//        List<Book> books = new ArrayList();O
		books.add(new Book(1, "Pinocho", "Pepeto"));
		books.add(new Book(2, "Java A Fondo", "AlfaOmega"));
		books.add(new Book(3, "Java 6 ", "Kathy Sierra"));
		books.add(new Book(4, "Genuino", "SinComentarios"));
		books.stream().forEach(book -> System.out.println(book.getTitulo()));

	}

}
