package clase8_9.comparable;

public class Alumno implements Comparable<Alumno> {

	private String nombre;
	private Integer edad;

	public Alumno() {
	}

	public Alumno(String nombre, Integer edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	@Override
	public int compareTo(Alumno o) {
		return this.getEdad() < o.getEdad() ? -1 : 0;
	}

}
