package clase8_9.comparable;

import java.util.Comparator;

public class AlumnoComparator implements Comparator<Alumno> {

	@Override
	public int compare(Alumno o1, Alumno o2) {
		return o1.getEdad() < o2.getEdad() ? 0 : -1;
	}

}
