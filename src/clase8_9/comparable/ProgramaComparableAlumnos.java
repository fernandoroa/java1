package clase8_9.comparable;

import java.util.ArrayList;
import java.util.Collections;

public class ProgramaComparableAlumnos {

	public static void main(String[] args) {
		ArrayList<Alumno> alumnos = new ArrayList<>();
		alumnos.add(new Alumno("Pablo", 12));
		alumnos.add(new Alumno("Ana", 30));
		alumnos.add(new Alumno("Ricardo", 18));
		// Descomentar luego de crear la clase AlumnoComparator
		// Collections.sort(alumnos, new AlumnoComparator());
		Collections.sort(alumnos);
		alumnos.stream().forEach(alumno -> System.out.println(alumno.getNombre()));

	}

}
