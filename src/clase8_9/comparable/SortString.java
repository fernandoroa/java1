package clase8_9.comparable;

import java.util.ArrayList;
import java.util.Collections;

public class SortString {
	
	
	public static void main(String[] args) {
		ArrayList<String> nombre = new ArrayList<>();
		nombre.add("Maria");
		nombre.add("Ana");
		nombre.add("Darian");
		nombre.add("Pedro");
		nombre.add("Berta");
		
		Collections.sort(nombre);
		System.out.println(nombre.toString());
		
	}

}
