package clase8_9.comparator;

import java.util.Comparator;

import clase6.ejercicios.taller.model.Persona;

public class OrdenarPersonaPorID implements Comparator<Persona> {

	@Override
	public int compare(Persona o1, Persona o2) {
		return o1.getId() - o2.getId();
	}

}
