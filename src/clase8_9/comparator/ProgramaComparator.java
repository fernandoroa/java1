package clase8_9.comparator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import clase6.ejercicios.taller.model.Persona;

public class ProgramaComparator {

	public static void main(String[] args) {
		List<Persona> otrasPersonas = Arrays.asList(
				new Persona(4, "F","Juana"), 
				new Persona(2, "M","Fernando"), 
				new Persona(1, "M","Mario"),
				new Persona(3, "M","Omar"));
		Collections.sort(otrasPersonas, new OrdenarPersonaPorID());
		otrasPersonas.stream().forEachOrdered(x->System.out.println(x.getNombre()));
	}

}
