package clase8_9.ejercicios.comparable;

public class Vehiculo implements Comparable<Vehiculo> {

	private String marca;
	private int anio;

	public Vehiculo(String marca, int anio) {
		super();
		this.marca = marca;
		this.anio = anio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	@Override
	public int compareTo(Vehiculo o) {
		boolean marcaB = !this.getMarca().equalsIgnoreCase(o.getMarca());
		boolean anioB = this.getAnio() < o.getAnio() ? true : false;
		return marcaB && anioB ? -1 : 0;
	}

}
