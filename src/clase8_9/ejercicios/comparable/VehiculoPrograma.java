package clase8_9.ejercicios.comparable;

import java.util.ArrayList;
import java.util.Collections;

public class VehiculoPrograma {
	
	public static void main(String[] args) {
		ArrayList<Vehiculo> vehiculos = new ArrayList<>();
		vehiculos.add(new Vehiculo("Chevrolet", 2016));
		vehiculos.add(new Vehiculo("Mazda", 2010));
		vehiculos.add(new Vehiculo("Honda", 2011));
		vehiculos.add(new Vehiculo("Mercedes", 2009));
		vehiculos.add(new Vehiculo("Toyota", 2001));
		vehiculos.add(new Vehiculo("Renault", 2019));
		
		Collections.sort(vehiculos);
		vehiculos.stream().forEach(vehiculo-> System.out.println(vehiculo.getMarca()));
		
	}
	

}
