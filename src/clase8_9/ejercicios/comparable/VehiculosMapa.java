package clase8_9.ejercicios.comparable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

public class VehiculosMapa {

	
	public static void main(String[] args) throws IOException {
		LinkedHashMap<Integer, String> mapas = new LinkedHashMap<>();
		
		ArrayList<Vehiculo> vehiculos = new ArrayList<>();
		vehiculos.add(new Vehiculo("Chevrolet", 2016));
		vehiculos.add(new Vehiculo("Mazda", 2010));
		vehiculos.add(new Vehiculo("Honda", 2011));
		vehiculos.add(new Vehiculo("Mercedes", 2009));
		vehiculos.add(new Vehiculo("Toyota", 2001));
		vehiculos.add(new Vehiculo("Renault", 2019));
		
		Collections.sort(vehiculos);
		for (Vehiculo vehiculo : vehiculos) {
			mapas.put(vehiculo.getAnio(), vehiculo.getMarca());
		}
		System.out.println(mapas.toString());
		
		
		File file = new File("Automoviles.txt");
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
		
		StringBuffer stringBuffer = new StringBuffer();
		for (Vehiculo vehiculo : vehiculos) {
			stringBuffer.append(vehiculo.getAnio()).append("\t").append(vehiculo.getMarca()).append("\n");
		}
		
		bufferedWriter.write(stringBuffer.toString());
		bufferedWriter.close();
	}
}
