package clase8_9.ejercicios.comparator;

public class Moto {

	private String marca;
	private Integer motor;
	private Integer a�o;
	private Integer velocidadMax;

	public Moto(String marca, Integer motor, Integer a�o, Integer velocidadMax) {
		super();
		this.marca = marca;
		this.motor = motor;
		this.a�o = a�o;
		this.velocidadMax = velocidadMax;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Integer getMotor() {
		return motor;
	}

	public void setMotor(Integer motor) {
		this.motor = motor;
	}

	public Integer getA�o() {
		return a�o;
	}

	public void setA�o(Integer a�o) {
		this.a�o = a�o;
	}

	public Integer getVelocidadMax() {
		return velocidadMax;
	}

	public void setVelocidadMax(Integer velocidadMax) {
		this.velocidadMax = velocidadMax;
	}

}
