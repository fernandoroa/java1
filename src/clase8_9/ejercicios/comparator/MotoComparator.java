package clase8_9.ejercicios.comparator;

import java.util.Comparator;

public class MotoComparator implements Comparator<Moto>{

	@Override
	public int compare(Moto o1, Moto o2) {
		return o1.getA�o() < o2.getA�o()? -1: 0;
	}
	
}
