package clase8_9.ejercicios.comparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class MotoPrograma {
	
	public static void main(String[] args) {
		ArrayList<Moto> motos = new ArrayList<Moto>();
		motos.add(new Moto("Zuzuki", 110, 2018, 150));
		motos.add(new Moto("Zanella", 110, 2017, 150));
		motos.add(new Moto("Honda", 300, 2016, 300));
		motos.add(new Moto("BMW", 110, 2015, 200));
		Collections.sort(motos, new MotoComparator());
		motos.stream().forEach(moto-> System.out.println(moto.getMarca()));
		Map<Object, Object> mapita = motos.stream().collect(Collectors.toMap(x -> x.getA�o(), x-> x.getMarca()));
		System.out.println(mapita);
	}

}
