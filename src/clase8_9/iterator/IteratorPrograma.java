/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.iterator;

import java.util.ArrayList;
import java.util.Iterator;

import clase7.set.Book;

/**
 *
 * @author Fernando
 */
public class IteratorPrograma {

    public static void main(String[] args) {
        ArrayList<Book> arrayList = new ArrayList<Book>();
        arrayList.add(new Book(1, "Guerra 2", "Sin autor"));
        arrayList.add(new Book(2, "Guerra 3", "Sin autor"));
        arrayList.add(new Book(3, "Guerra 1", "Sin autor"));

        Iterator iterator = arrayList.iterator();
        while(iterator.hasNext()){
            Book book = (Book) iterator.next();
            System.out.println("titulo: "+book.getTitulo());
        }
        
        
    }

}
