/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.map;

import java.util.HashMap;

/**
 *
 * @author fernando.melendez
 */
public class HashMapPrograma {

    public static void main(String[] args) {
        HashMap<Integer, String> hashMap = new HashMap<Integer, String>();

        hashMap.put(0, "value1");

        hashMap.put(1, "value2");

        hashMap.put(2, "value3");

        System.out.println(hashMap);

        hashMap.remove(0);

        System.out.println(hashMap);
    }
}
