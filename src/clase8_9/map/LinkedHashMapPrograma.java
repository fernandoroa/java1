/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.map;

import java.util.LinkedHashMap;

/**
 *
 * @author fernando.melendez
 */
public class LinkedHashMapPrograma {

    public static void main(String[] args) {
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();

        linkedHashMap.put(0, "value1");
        linkedHashMap.put(1, "value2");
        linkedHashMap.put(2, "value3");

        System.out.println(linkedHashMap);

        linkedHashMap.remove(0);

        System.out.println(linkedHashMap);
    }
}
