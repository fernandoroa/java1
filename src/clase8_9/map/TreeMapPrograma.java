/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.map;

import java.util.TreeMap;

/**
 *
 * @author Fernando
 */
public class TreeMapPrograma {

    public static void main(String[] args) {
        TreeMap<String, Integer> treeMap = new TreeMap<String, Integer>();

        treeMap.put("key1", 111);

        treeMap.put("key3", 222);

        treeMap.put("key2", 333);

        System.out.println(treeMap);

        treeMap.remove("key1");

        System.out.println(treeMap);
    }

}
