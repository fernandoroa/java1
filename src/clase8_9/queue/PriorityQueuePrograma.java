/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.queue;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author Fernando
 */
public class PriorityQueuePrograma {

    public static void main(String[] args) {
        Comparator<String> comparator = new StringLengthComparator();

        PriorityQueue<String> queue = new PriorityQueue<>(10, comparator);

        queue.add("short");
        queue.add("very long indeed");
        queue.add("medium");

        while (!queue.isEmpty()) {
            System.out.println(queue.remove());
        }
    }
}
