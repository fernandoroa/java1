/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase8_9.queue;

import java.util.Comparator;

/**
 *
 * @author Fernando
 */
class StringLengthComparator implements Comparator<String> {

    public StringLengthComparator() {
    }

    @Override
    public int compare(String o1, String o2) {
        return o1.equalsIgnoreCase(o2) ? 1 : 0;
    }

}
